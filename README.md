# libwurscht

libwurscht is the Open Source library for all Wurscht clients written in Kotlin

---

## How to use

It's a Maven Project, so you can simply add it as dependency to your project in your ```pom.xml```:

```xml
<dependencies>

    ...

    <dependency>
        <groupId>de.wagner.wurscht</groupId>
        <artifactId>libwurscht</artifactId>
        <version>1.1</version>
    </dependency>
</dependencies>
```

or if you use Gradle in your ```build.gradle.kts```:

```kotlin
dependencies {
    implementation("de.wagner.wurscht:libwurscht:1.1")
}
```

Note: This Project is not uploaded to remote Maven Repository like Maven-Central, so you have run ```git clone https://gitlab.bixilon.de/elias/libwurscht.git``` and ```mvn clean install``` first
