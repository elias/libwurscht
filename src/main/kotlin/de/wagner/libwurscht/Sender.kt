package de.wagner.libwurscht

import java.net.DatagramPacket
import java.net.DatagramSocket

/*
Sender class with constructor parameters IP address, port and sender name
 */

class Sender constructor(private val socket: DatagramSocket) {

    fun senden(packet: WurschtPacket): WurschtPacket {
        socket.send(packet.getDatagramPacket())

        var byteArray = ByteArray(22)
        var answer = DatagramPacket(byteArray, 22)

        var wurschtAnswer: WurschtPacket
        do {
            socket.receive(answer)
            wurschtAnswer = WurschtPacket(answer)
        }while (wurschtAnswer.type != PacketType.ANSWER)

        return wurschtAnswer
    }

}