package de.wagner.libwurscht

import java.net.DatagramPacket
import java.net.DatagramSocket

/*
Sender class with constructor parameters port and name to answer
 */

class Receiver(val socket: DatagramSocket, val receiverName: String){


    fun receive(): WurschtPacket {

        var message = DatagramPacket(ByteArray(165), 165)
        socket.receive(message)
        val packet = WurschtPacket(message)

        val answerArray = WurschtPacket(receiverName, packet.ip, packet.port)
        socket.send(answerArray.getDatagramPacket())

        return packet
    }
}