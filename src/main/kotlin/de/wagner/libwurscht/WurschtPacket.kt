package de.wagner.libwurscht

import java.net.DatagramPacket
import java.net.InetAddress

class WurschtPacket{
    val packetArray: ByteArray
    val ip: InetAddress
    val port: Int
    val type: PacketType

    // Constructor for Message Packets
    constructor(name: String, message: String, broadcastIp: InetAddress, broadcastPort: Int) {
        ip = broadcastIp
        port = broadcastPort
        type= PacketType.NOTIFICATION

        val nameArray = name.toByteArray(Charsets.UTF_8)
        val messageArray = message.toByteArray(Charsets.UTF_8)
        packetArray = byteArrayOf(0x0000000, nameArray.size.toByte(), messageArray.size.toByte()) + nameArray.copyOf(nameArray.size.coerceAtMost(20)) + messageArray.copyOf(messageArray.size.coerceAtMost(140))
    }

    // Constructor for Answer Packets
    constructor(name: String, senderIp: InetAddress, senderPort: Int) {
        ip = senderIp
        port = senderPort
        type = PacketType.ANSWER

        val nameArray = name.toByteArray(Charsets.UTF_8)
        packetArray = byteArrayOf(0x00000001) + nameArray.copyOf(nameArray.size.coerceAtMost(20))
    }


    constructor(packet: DatagramPacket){
        ip = packet.address
        port = packet.port

        packetArray = packet.data

        when(packetArray[0]){
            0.toByte() -> type = PacketType.NOTIFICATION
            1.toByte() -> type = PacketType.ANSWER
            else -> type = PacketType.UNKNOWN
        }
    }

    fun getDatagramPacket(): DatagramPacket{
        return DatagramPacket(packetArray, packetArray.size, ip, port)
    }

    fun getName(): String {
        val nameArray: ByteArray
        when(type) {
            PacketType.NOTIFICATION -> {
                nameArray = packetArray.copyOfRange(3, packetArray[1].toInt() + 3)
                return nameArray.copyOf(nameArray.size.coerceAtMost(20))
                    .toString(Charsets.UTF_8)
            }

            PacketType.ANSWER -> {
            nameArray = packetArray.copyOfRange(1, packetArray.size)
                return nameArray.copyOf(nameArray.size.coerceAtMost(20)).toString(Charsets.UTF_8)
            }
            else -> throw Exception("The received Packet is broken!")
        }
    }

    fun getMessage(): String {
        val messageArray = packetArray.copyOfRange(packetArray[1].toInt() + 3, 3 + packetArray[1] + packetArray[2].toInt())
        return messageArray.copyOf(messageArray.size.coerceAtMost(140)).toString(Charsets.UTF_8)
    }

}