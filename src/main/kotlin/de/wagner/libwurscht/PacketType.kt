package de.wagner.libwurscht

enum class PacketType {
    NOTIFICATION,
    ANSWER,
    UNKNOWN
}