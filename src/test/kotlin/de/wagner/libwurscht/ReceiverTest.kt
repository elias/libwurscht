package de.wagner.libwurscht

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import java.net.DatagramSocket

class ReceiverTest {

    val socket = DatagramSocket(56788)
    val receiver = Receiver(socket, "Elias")
    var packet: WurschtPacket? = null

    fun receive() {
        packet = receiver.receive()

        println("Empfangen: ${packet!!.getName()}: ${packet!!.getMessage()}")
    }
}