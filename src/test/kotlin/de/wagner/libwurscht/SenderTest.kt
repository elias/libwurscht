package de.wagner.libwurscht

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import java.net.DatagramSocket
import java.net.InetAddress

class SenderTest {

    fun senden() {
        val socket = DatagramSocket()
        val ip: InetAddress = InetAddress.getByName("192.168.2.255")
        val sender = Sender(socket)
        val packet = WurschtPacket("Elias", "Test", ip, 56788)

        println("Wird gesendet...")
        val answer = sender.senden(packet)

        println("Gesendet: ${packet.getName()}: ${packet.getMessage()}")
        println("Answers: ${answer.getName()}")
    }
}