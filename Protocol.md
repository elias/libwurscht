# Protocol specification of libwurscht

## Message Paket

| Byte index       | Content          | Description                                                |
|------------------|------------------|------------------------------------------------------------|
| 0                | 0                | Defines the Paket Type: 0 is for Message Pakets            |
| 1                | name length      | Length of the name field beginning with index 3            |
| 2                | message length   | Length of the message field beginning after the name field |
| 3 - max 23       | name             | Name String in UTF-8                                       |
| max 24 - max 164 | message          | Message String in UTF-8                                    |

---

## Answer Paket

| Byte index       | Content          | Description                                                |
|------------------|------------------|------------------------------------------------------------|
| 0                | 1                | Defines the Paket Type: 1 is for Answer Pakets            |
| 1 - max 21       | name             | Name String in UTF-8                                       |
